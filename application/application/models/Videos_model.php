<?php

class Videos_model extends CI_Model 
{

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
		
		public function get_search_videos($ids)
		{
			//var_dump($ids);
			$response = array();
			$photo = 'http://wstest.beaconhouse.net/sotportal/index.php/';
			$dimensions = $this->db->query("SELECT * FROM bssdata.event_videos where schedule_id IN ('$ids')");
			if(!$dimensions) 
			{
				$this->log_db_error($this->db->error(), 'event_videos', '');
				
				 return $response;
			}
			$response = $dimensions->result_array();

			return $response;
		}
		
		public function get_all_videos()
		{
			$response = array();
			$photo = 'http://wstest.beaconhouse.net/sotportal/index.php/';
			$dimensions = $this->db->query('SELECT * FROM bssdata.event_videos');
			if(!$dimensions) 
			{
				$this->log_db_error($this->db->error(), 'dimensions', '');
				
				 return $response;
			}
			$response = $dimensions->result_array();

			return $response;
		}
		
		public function get_events($user_id, $refresh_time = '') 
		{
			$response = array();
			
			if($refresh_time AND $refresh_time != '') {
				
				
			}
			
			$events = @$this->db->query("SELECT e.event_id,
										  e.event_title,
										  e.event_desc,
										  e.active,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM event_evaluation
										  WHERE event_id              = e.event_id
										  AND upper(NVL(status, 'N')) = 'Y'
										  ) evaluation_exists,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM event_evaluation
										  WHERE event_id              = e.event_id
										  AND upper(NVL(status, 'N')) = 'Y'
										  ) evaluation_active,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM EVENT_EVALUATION EV,
											EVENT_EVALUATION_RESPONSE EVR
										  WHERE EV.EVALUATION_ID = EVR.EVALUATION_ID
										  AND EV.EVENT_ID        = e.event_id
										  AND EVR.USER_ID        = '$user_id'
										  AND NVL(EVR.schedule_id, 0) = 0
										  ) evaluation_submitted,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM EVENT_USER_SOURCE
										  WHERE EVENT_ID        = e.event_id
										  AND USER_ID        = '$user_id'
										  ) source_updated
										  /*, TO_CHAR(e.start_date, 'ddth Month') start_date,
										  TO_CHAR(e.end_date, 'ddth Month') end_date,
										  TO_CHAR(e.start_date, 'YYYY') event_year,
										  TO_CHAR(e.start_date, 'ddth Month')
										  || ' '
										  || TO_CHAR(e.end_date, 'ddth Month')
										  || ' '
										  || TO_CHAR(e.start_date, 'YYYY') display_date,
										  TO_CHAR(e.start_date, 'dd/mm/yyyy') from_date,
										  TO_CHAR(e.end_date, 'dd/mm/yyyy') to_date*/
										FROM BSSDATA.events e
										WHERE upper(NVL(e.visible_to_mob_app, 'Y')) = 'Y'
										AND e.event_catagory = 'SOT'
										ORDER BY e.event_id DESC");			
			if(!$events) 
			{
				$this->log_db_error($this->db->error(), 'get_events', '');
				
				 return $response;
			}
			
			if($events AND $events->num_rows() > 0) {
				
				foreach ($events->result() as $event)
				{
					$event_id = $event->EVENT_ID;
					
					$event_dates = $this->db->query("SELECT NVL(start_date, event_start_date) start_date,
														  NVL(end_date, event_end_date) end_date,
														  CASE
															WHEN REPLACE(display_date,'-', '') IS NULL
															THEN event_display_date
															ELSE display_date
														  END display_date
														FROM
														  (SELECT TO_CHAR(MIN(es.activity_date), 'ddth Month') start_date,
															TO_CHAR(MAX(es.activity_date), 'ddth Month') end_date,
															TO_CHAR(MIN(es.activity_date), 'YYYY') event_year,
															TO_CHAR(MIN(es.activity_date), 'ddth Month')
															|| ' '
															|| TO_CHAR(MAX(es.activity_date), 'ddth Month')
															|| ' '
															|| TO_CHAR(MIN(es.activity_date), 'YYYY') display_date_,
															TRIM(TO_CHAR(MIN(es.activity_date), 'FmMonth')
															|| ' '
															|| TO_CHAR(MIN(es.activity_date), 'dd')
															|| ' - '
															|| TO_CHAR(MAX(es.activity_date), 'dd')) display_date,
															TO_CHAR(MIN(es.activity_date), 'dd/mm/yyyy') from_date,
															TO_CHAR(MAX(es.activity_date), 'dd/mm/yyyy') to_date,
															TO_CHAR(MIN(e.start_date), 'ddth Month') event_start_date,
															TO_CHAR(MAX(e.end_date), 'ddth Month') event_end_date,
															TO_CHAR(MIN(e.start_date), 'FmMonth')
															|| ' '
															|| TO_CHAR(MIN(e.start_date), 'dd')
															|| ' - '
															|| TO_CHAR(MAX(e.end_date), 'dd') event_display_date
														  FROM bssdata.events e,
															bssdata.event_schedule es
														  WHERE e.event_id = es.event_id (+)
														  AND e.event_id   = '$event_id'
														  )")->row();
					
					/*$taglines = $this->db->query("SELECT COUNT(DISTINCT es.schedule_id) sessions,
													  (SELECT COUNT(DISTINCT p.participant_id)
														  FROM bssdata.event_participant p,
															bssdata.event_participants_shortlist sh,
															bssdata.event_schedule sc,
															bssdata.event_schedule_detail sd
														  WHERE p.participant_id = sh.participant_id
														  AND sh.participant_id  = sd.participant_id
														  AND sh.event_id        = sc.event_id
														  AND sh.event_id        = '$event_id'
														  AND NVL(sh.status,'N') = 'Y'
														  --AND p.publish_profile  = 1
													  ) speakers,
													  COUNT(DISTINCT es.activity_date) days,
													  es.event_id
													FROM BSSDATA.event_schedule es,
													  BSSDATA.event_schedule_detail ed
													WHERE es.schedule_id = ed.schedule_id
													AND es.event_id      = '$event_id'
													GROUP BY es.event_id")->row();*/
					
					$taglines = $this->db->query("SELECT COUNT(DISTINCT es.schedule_id) sessions,
													  (SELECT COUNT(DISTINCT p.participant_id)
													  FROM bssdata.event_participant p,
														bssdata.event_schedule sc,
														bssdata.event_schedule_detail sd
													  WHERE sc.schedule_id = sd.schedule_id
													  AND p.participant_id = sd.participant_id
													  AND sc.event_id      = '$event_id'
													  ) speakers,
													  COUNT(DISTINCT es.activity_date) days,
													  es.event_id
													FROM BSSDATA.event_schedule es,
													  BSSDATA.event_schedule_detail ed
													WHERE es.schedule_id = ed.schedule_id
													AND es.event_id      = '$event_id'
													GROUP BY es.event_id")->row();
													
					$session_tagline = 'No session found.';
					$speaker_tagline = 'No Speaker Found';
					
					if($taglines) {
						$session_tagline = $taglines->SESSIONS . ' sessions in ' . $taglines->DAYS . ' days';
						$speaker_tagline = $taglines->SPEAKERS . ' speakers';
					}
					
					$data = array(
								'event_id' 			=> $event->EVENT_ID,
								'event_title'		=> $event->EVENT_TITLE,
								'event_desc'		=> $event->EVENT_DESC,
								'active'			=> $event->ACTIVE,
								'start_date'		=> $event_dates->START_DATE,
								'end_date'			=> $event_dates->END_DATE,
								'display_date'		=> $event_dates->DISPLAY_DATE,
								'evaluation_exists'	=> $event->EVALUATION_EXISTS,
								'evaluation_active'	=> $event->EVALUATION_ACTIVE,
								'evaluation_submitted' => $event->EVALUATION_SUBMITTED,
								'info_source_updated'  => $event->SOURCE_UPDATED,
								'locations'			=> array(),
								//'programs'			=> $this->get_programs($event_id, $user_id),
								'timeslots'			=> $this->get_programs_timeslots($event_id),
								'activity_types' 	=> $this->get_event_activity_types($event_id),
								'dimensions'		=> $this->get_event_themes($event_id),
								'venues'			=> $this->get_event_venues($event_id),
								'event_dates'		=> array(),
								'session_tagline'	=> $session_tagline,
								'speaker_tagline'	=> $speaker_tagline,
								'fullsteam_desc' 	=> '<p>STEAM, an interdisciplinary spin on STEM that includes an "A" for art, is an integral part of influencing kids\' interest in STEM by allowing kids to explore these subjects through hands-on making. STEAM is a popular movement that was founded by Georgette Yakman in 2007 to promote and integrate design and art in STEM fields. She defined the movement as "Science and Technology, interpreted through Engineering and the Arts, all based in elements of Mathematics."</p><p>The STEAM movement has inspired new school systems, companies and organizations to create new and engaging learning experiences that have captured kids\' interest. STEAM compliments both sides of the brain. By incorporating all components into a lesson, unit, or project, STEAM education naturally differentiates instruction. It appeals to learning styles, interests, and learning capacity. This creates more ideas from the students, and allows them more creative input.</p>',
								'last_refresh_time' => $this->get_current_db_time()
							);
					
					$locations = @$this->db->query("SELECT loc.*,
													  c.city_name
													FROM BSSDATA.event_locations loc,
													  COMMON.com_cities c
													WHERE loc.city_id 	= c.city_id
													AND loc.status    	= 'Y'
													AND loc.event_id  	= '$event_id'
													--AND loc.location_id = 1
													ORDER BY loc.location_id ASC");
					if(!$locations) 
					{
						$this->log_db_error($this->db->error(), 'get_events', '');
					}
					
					if($locations AND $locations->num_rows() > 0) {
						
						foreach($locations->result() as $location) {
							
							$loc = array(
										'location_id' 	=> $location->LOCATION_ID,
										'location_name' => $location->LOCATION_NAME,
										'city_id' 		=> $location->CITY_ID,
										'city_name' 	=> $location->CITY_NAME,
										'status' 		=> $location->STATUS,
										'lat' 			=> $location->G_LAT,
										'long' 			=> $location->G_LONG,
										'lat_long'		=> $location->G_LAT.','.$location->G_LONG
									);	
							
							array_push($data['locations'], $loc);		
						}
					}
					
					$event_dates = @$this->db->query("SELECT DISTINCT TO_CHAR(activity_date, 'dd/mm/yyyy') program_date,
														TO_CHAR(activity_date, 'fmDay DD Mon, YYYY') event_display_date
													FROM event_schedule
													WHERE event_id     = '$event_id'
													AND activity_date IS NOT NULL
													ORDER BY program_date");
					if(!$event_dates) 
					{
						$this->log_db_error($this->db->error(), 'get_events', '');
					}
					
					if($event_dates AND $event_dates->num_rows() > 0) {
						
						$day_index = 1;
						foreach($event_dates->result() as $event_date) {
							
							$date = array(
										'event_day' 			=> $day_index,
										'event_date' 			=> $event_date->PROGRAM_DATE,
										'event_display_date' 	=> $event_date->EVENT_DISPLAY_DATE,
										'timeslots'				=> $this->get_programs_timeslots($event_id,$event_date->PROGRAM_DATE) 
									);	
							
							array_push($data['event_dates'], $date);
							$day_index++;	
						}
					}
					
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function get_event_activity_types($event_id = '', $refresh_time = '') 
		{
			$response = array();
			
			if($event_id) {			
				$activity_types = @$this->db->query("SELECT DISTINCT typ.activity_type_id,
													  typ.activity_desc
													FROM event_activity act,
													  event_activity_types typ
													WHERE act.activity_type_id = typ.activity_type_id
													AND act.event_id           = '$event_id'
													ORDER BY typ.activity_desc");
			} else {
				$activity_types = @$this->db->query("SELECT DISTINCT typ.activity_type_id,
													  typ.activity_desc
													FROM event_activity_types typ
													ORDER BY typ.activity_desc");
			}
			
			if(!$activity_types) 
			{
				$this->log_db_error($this->db->error(), 'get_event_activity_types', '');
				
				 return $response;
			}
			
			if($activity_types AND $activity_types->num_rows() > 0) {
				
				foreach ($activity_types->result() as $activity_type)
				{					
					$data = array(
								'activity_type_id' 	=> $activity_type->ACTIVITY_TYPE_ID,
								'activity_desc'		=> $activity_type->ACTIVITY_DESC
							);
							
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function get_event_themes($event_id = '', $refresh_time = '') 
		{
			$response = array();
			
			if($event_id) {			
				$themes = @$this->db->query("SELECT theme_id,
											  theme_desc,
											  short_name,
											  color_code,
											  text_color_code
											FROM event_theme
											WHERE event_id = '$event_id'
											ORDER BY theme_desc");
			} else {
				$themes = @$this->db->query("SELECT theme_id,
											  theme_desc,
											  short_name,
											  color_code,
											  text_color_code
											FROM event_theme
											--WHERE event_id = '2'
											ORDER BY theme_desc");
			}
			
			if(!$themes) 
			{
				$this->log_db_error($this->db->error(), 'get_event_themes', '');
				
				 return $response;
			}
			
			if($themes AND $themes->num_rows() > 0) {
				
				foreach ($themes->result() as $theme)
				{					
					$data = array(
								'theme_id' 			=> $theme->THEME_ID,
								'theme_desc'		=> $theme->THEME_DESC,
								'short_name'		=> $theme->SHORT_NAME,
								'color_code'		=> $theme->COLOR_CODE,
								'text_color_code'	=> $theme->TEXT_COLOR_CODE
							);
							
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function get_event_venues($event_id = '', $refresh_time = '') 
		{
			$response = array();
			
			$venues = @$this->db->query("SELECT loc.event_id,
										  v.location_id,
										  v.city_id,
										  c.city_name,
										  v.venue_id,
										  v.venue_title,
										  v.sort_order
										FROM BSSDATA.event_locations loc,
										  COMMON.com_cities c,
										  event_venue v
										WHERE loc.city_id   = c.city_id
										AND loc.location_id = v.location_id
										AND loc.status      = 'Y'
										--AND loc.location_id = 1
										AND loc.event_id    = '$event_id'
										ORDER BY v.sort_order ASC");
			
			if(!$venues) 
			{
				$this->log_db_error($this->db->error(), 'get_event_venues', '');
				
				 return $response;
			}
			
			if($venues AND $venues->num_rows() > 0) {
				
				foreach ($venues->result() as $venue)
				{					
					$data = array(
								'location_id'		=> $venue->LOCATION_ID,
								'venue_id' 			=> $venue->VENUE_ID,
								'venue_title'		=> $venue->VENUE_TITLE,
								'sort_order'		=> $venue->SORT_ORDER
							);
							
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function get_programs_timeslots($event_id, $activity_date = '') 
		{
			$response = array();
			
			if($event_id) {
				
				if($activity_date != '') 
				{
					$timings = @$this->db->query("SELECT TO_CHAR(activity_date, 'dd/mm/yyyy') activity_date,
												  rtrim (xmlagg (xmlelement (e, schedule_id
												  || ',')).extract ('//text()'), ',') schedule_ids
												FROM
												  (SELECT activity_date,
													start_time,
													end_time,
													schedule_id
												  FROM event_schedule
												  WHERE event_id  = '$event_id'
												  AND activity_date = to_date('$activity_date', 'dd/mm/yyyy')
												  AND start_time IS NOT NULL
												  AND end_time   IS NOT NULL
												  )
												GROUP BY activity_date
												ORDER BY activity_date");
				} else {
					
					$timings = @$this->db->query("SELECT TO_CHAR(activity_date, 'dd/mm/yyyy') activity_date,
													  rtrim (xmlagg (xmlelement (e, schedule_id
													  || ',')).extract ('//text()'), ',') schedule_ids
													FROM
													  (SELECT activity_date,
														start_time,
														end_time,
														schedule_id
													  FROM event_schedule
													  WHERE event_id  = '$event_id'
													  --AND location_id = 1
													  AND start_time IS NOT NULL
													  AND end_time   IS NOT NULL
													  )
													GROUP BY activity_date
													ORDER BY activity_date");
				}
				
				if(!$timings) 
				{
					$this->log_db_error($this->db->error(), 'get_programs_timeslots', '');
					
					 return $response;
				}
			
				if($timings AND $timings->num_rows() > 0) {
					
					foreach ($timings->result() as $timing)
					{					
						$activity_date 	= $timing->ACTIVITY_DATE;
						$schedule_ids	= $timing->SCHEDULE_IDS;
						
						$data = array(
									'activity_date' 	=> $activity_date,
									'activity_time'		=> array()
								);
						
						$timeslots = @$this->db->query("SELECT DISTINCT start_time,
														  end_time,
														  start_time
														  || '-'
														  || end_time display_time
														FROM event_schedule
														WHERE schedule_id IN ($schedule_ids)
														ORDER BY start_time,
														  end_time");
						if($timeslots AND $timeslots->num_rows() > 0) {
							
							foreach($timeslots->result() as $timeslot) {
								
								$slots = array(
											'start_time'	=> $timeslot->START_TIME,
											'end_time'		=> $timeslot->END_TIME,
											'display_time'	=> $timeslot->DISPLAY_TIME
										 );	
								array_push($data['activity_time'], $slots);
							}
						}
						array_push($response, $data);
					}
				}
			}
			
			return $response;
		}
		
		#log DB errors
		public function log_db_error($error, $service_name, $remarks = '') 
		{
			if($error AND count($error) > 0) {
				
				$db2= $this->load->database('trans', TRUE);	
				
				$data['LOG_ID'] 		= $db2->query('SELECT EVENT_APP_SEQ_ERROR_LOGID.nextVal AS LOG_ID FROM dual')->row()->LOG_ID;
				$data['APP_NAME']		= 'STO Events App';
				$data['SERVICE_NAME']	= $service_name;
				$data['ERROR_CODE']		= $error['code'];
				$data['MESSAGE']		= $error['message'];
				$data['OFFSET']			= $error['offset'];
				$data['SQLTEXT']		= $error['sqltext'];
				$data['REMARKS']		= $remarks;
				
				@$db2->insert('EVENT_APP_ERRORS_LOG', $data);
				
				$message  = '<p> SERVICE NAME : ' . $service_name . '</p>';
				$message .= '<p> ERROR CODE : ' . $error['code'] . '</p>';
				$message .= '<p> ERROR MESSAGE : ' . $error['message'] . '<p>';
				$message .= '<p> SQL STATEMENT : <br><br> ' . $error['sqltext'] . '<p>';
				
				@$this->send_email('samee.ullah@bh.edu.pk, shahzad.rafique@bh.edu.pk, jabbar.khan@bh.edu.pk', 'SOT EVENTS APP DATABASE ERROR', $message);
			}
			
		}
}
?>