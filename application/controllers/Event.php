<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Event extends REST_Controller 
{
	private $api_key 		= "X5Ne0km7852Q1ykny9FfcIK5y9kVV5v6";
	private $api_secret 	= "Q1X5NeknkyV5v6Vkm78y9FfcI0K5y952";
	private $auth_token;
	
	private $signup_params 	= array('email');
	private $event_id;
	private $schedule_id;
	private $user_id;
	private $user_password;
	private $user_email;
	private $sms_response;
	private $request_id;
	private $email_address;
	private $notification_id;
	private $last_refresh_time;
	

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct() 
	{
		parent::__construct();
		
		ini_set('always_populate_raw_post_data', -1);
		// Load api model by default
		$this->load->model('api_model', '', TRUE);
		$this->load->model('event_model', '', TRUE);

        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		
		header('Access-Control-Allow-Origin: *');        
		header("Access-Control-Allow-Headers: api_key, api_secret, device_token, device_identifier, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");        
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");        
		$method = $_SERVER['REQUEST_METHOD'];        
		if ($method == "OPTIONS") {            
			die();        
		}	
	}
	
	public function index_get()
	{
		echo 'test';
		//$this->load->view('feedback_form_view');
	}
	
	public function all_get($event_id = null)
	{

		$response 	= array();
		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			if(!validate_get($event_id)){
				$response['success'] = 0;
				$response['message'] = 'Event id must be integer';
				$this->response($response, 404);
			}

			$result =  $this->event_model->get($event_id);
			$response['result'] = array('events' => $result);
			$response['success'] = 1;
			$response['message'] = 'List of events.';
			$this->response($response, 200); // 200 being the HTTP response code
		}else{
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 404);
		}
	
	}

	public function event_videos_get($event_id)
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{

			if(!validate_get($event_id)){
				$response['success'] = 0;
				$response['message'] = 'Event id must be integer';
				$this->response($response, 404);
			}

			if(!$event_id){
				$response['success'] = 0;
				$response['message'] = 'Event id is require.';
				$this->response($response, 404);
			}

			$events =  $this->event_model->get($event_id);

			$videos	=  $this->event_model->get_event_videos($event_id);
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
						
			$response['result'] = array('events' => $events,'videos' => $videos,  'last_refresh_time' => $this->last_refresh_time, 'sources' => $this->api_model->get_sources());
			$response['success'] = 1;
			$response['message'] = 'List of events.';
			$this->response($response, 200); // 200 being the HTTP response code
		
		}else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 404);
		}
	
	}
	

	public function check_event_get($check)
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{

			if(!$check){
				$response['success'] = 0;
				$response['message'] = 'Event id is require.';
				$this->response($response, 404);
			}

			if(!validate_string($check)){
				$response['success'] = 0;
				$response['message'] = 'Event id must be integer';
				$this->response($response, 404);
			}

			$events =  $this->event_model->get_events($check);
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
						
			$response['result'] = array('events' => $events, 'last_refresh_time' => $this->last_refresh_time, 'sources' => $this->api_model->get_sources());
			$response['success'] = 1;
			$response['message'] = 'List of events.';
			$this->response($response, 200); // 200 being the HTTP response code
		
		}else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 404);
		}
	
	}

	public function event_type_get()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{

			
			$pre =  $this->event_model->get_events('pre');
			$current =  $this->event_model->get_events('current');
			$past =  $this->event_model->get_events('past');
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
						
			$response['result'] = array('pre_events' => $pre,'currents_events' => $current,'past_events' => $past, 'last_refresh_time' => $this->last_refresh_time, 'sources' => $this->api_model->get_sources());
			$response['success'] = 1;
			$response['message'] = 'List of events.';
			$this->response($response, 200); // 200 being the HTTP response code
		
		}else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 404);
		}
	
	}

	private function verify_api_key($key, $secret)
	{
	
		if($this->api_key == $key and $this->api_secret == $secret){
			
			return true;
			
		}else{
			
			if($key == 'beams' and $secret == 'app'){
				return true;
			} else {				
				$response['success'] = 0;
				$response['message'] = 'Invalid API key.';
				$this->response($response, 200);
			}
		}
		
		/*if($this->api_key != $key){
			$response['success'] = 0;
			$response['message'] = 'Invalid API key.';
			$this->response($response, 200);
		}else{
			return true;
		}*/
	   
	}
	
	private function validate_post($array_to_validate=array())
	{
	
		if($array_to_validate && count($array_to_validate))
		{
			foreach($array_to_validate as $ele)
			{
				if(empty($this->post($ele))){
					$response['success'] = 0;
					$response['message'] = 'Value missing for '.$ele;
					$this->response($response, 200);
				}
			}
		}
	}

}
