<?php
//application/controllers/Programs.php


// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'libraries/REST_Controller.php';

class Programs extends REST_Controller
{
	
	private $api_key 		= "X5Ne0km7852Q1ykny9FfcIK5y9kVV5v6";
	private $api_secret 	= "Q1X5NeknkyV5v6Vkm78y9FfcI0K5y952";
	private $auth_token;
	
	private $signup_params 	= array('email');
	private $event_id;
	private $schedule_id;
	private $user_id;
	private $user_password;
	private $user_email;
	private $sms_response;
	private $request_id;
	private $email_address;
	private $notification_id;
	private $last_refresh_time;
	
	
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
		
		ini_set('always_populate_raw_post_data', -1);
		// Load api model by default
		$this->load->model('programs_model', '', TRUE);

        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		
		header('Access-Control-Allow-Origin: *');        
		header("Access-Control-Allow-Headers: api_key, api_secret, device_token, device_identifier, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");        
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");        
		$method = $_SERVER['REQUEST_METHOD'];        
		if ($method == "OPTIONS") {            
			die();        
		}
		//ssl_redirect();	
    }
	
	# List of programs of an event
	public function programs_get() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_get(array('event_id', 'user_id'));
			
			$refresh_time = $this->post('last_refresh_time');
			
			$result = $this->programs_model->get_programs($this->input->get('event_id'), $this->input->get('user_id'), $refresh_time);
			
			$data = array(
			array(
			'cust_group' => 6,
			'price' => 13.21,
			'price_qty' => 5
			),
			array(
			'cust_group' => 8,
			'price' => 15.25,
			'price_qty' => 4
			),
			array(
			'cust_group' => 8,
			'price' => 12.75,
			'price_qty' => 10
			)
			);
			
			//$findKey = $this->search_revisions($result,'A Symbiotic Future', 'theme_desc');
			//print_r($findKey);
			
			//$this->searchForId('Panel Discussion', $result)
			//die();
			
			$this->last_refresh_time = $this->programs_model->get_current_db_time();
			
			if($result) {
				$response['result']  = array('programs' => $result, 'last_refresh_time' => $this->last_refresh_time);
				$response['success'] = 1;
				$response['message'] = 'List of programs.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']  = array();
				$response['success'] = 1;
				$response['message'] = 'Information Not Yet Available.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	private function verify_api_key($key, $secret)
	{
	
		if($this->api_key == $key and $this->api_secret == $secret){
			
			return true;
			
		}else{
			
			if($key == 'beams' and $secret == 'app'){
				return true;
			} else {				
				$response['success'] = 0;
				$response['message'] = 'Invalid API key.';
				$this->response($response, 200);
			}
		}
		
		/*if($this->api_key != $key){
			$response['success'] = 0;
			$response['message'] = 'Invalid API key.';
			$this->response($response, 200);
		}else{
			return true;
		}*/
	   
	}
	
	# validate post parameters
	private function validate_post($array_to_validate=array())
	{	
		if($array_to_validate && count($array_to_validate))
		{
			foreach($array_to_validate as $ele){
				if(empty($this->post($ele))){
					$response['success'] = 0;
					$response['message'] = 'Value missing for '.$ele;
					$this->response($response, 200);
				}
			}
		}
	}
	
	// validate post parameters
	private function validate_get($array_to_validate=array())
	{	
		if($array_to_validate && count($array_to_validate)){
			foreach($array_to_validate as $ele){
				if(empty($this->get($ele))){
					$response['success'] = 0;
					$response['message'] = 'Value missing for '.$ele;
					$this->response($response, 200);
				}
			}
		}
	}
	
	function search_revisions($dataArray, $search_value, $key_to_search) {
        // This function will search the revisions for a certain value
        // related to the associative key you are looking for.
        $keys = array();
        foreach ($dataArray as $key => $cur_value) {
            if ($cur_value[$key_to_search] == $search_value) {
                $keys[] = $key;
            }
        }
        return $keys;
    }
}
?>