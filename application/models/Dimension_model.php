<?php

class Dimension_model extends CI_Model 
{

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
		
		public function get($theme_id = null)
		{

			$response = array();
			$query  = 'SELECT * From V_EVENT_DIMENSIONS';
			if($theme_id != null){
				$query = $query.' WHERE THEME_ID = '.$theme_id;
			}else{
				$query = $query.' ORDER BY THEME_ID';
			}
			
			$dimensions = $this->db->query($query);

			if(!$dimensions) 
			{
				$this->log_db_error($this->db->error(), 'dimensions', '');
				 return $response;
			}
			if($theme_id != null){
				$response = $dimensions->row_array();
			}else{
				return $dimensions->result_array();
			}
			return $response;
        }


        public function dimension_videos($theme_id) 
		{

            $response = array();
        	$query  = "SELECT * 
			FROM EVENT_THEME t , event_schedule s, event_videos v 
            WHERE v.schedule_id  = s.schedule_id 
			AND  s.theme_id = t.theme_id
			AND v.status = 'Y'  
			AND t.theme_id  = ".$theme_id;
			$dimensions = $this->db->query($query);

			if(!$dimensions) 
			{
				$this->log_db_error($this->db->error(), 'dimension_videos', '');
				return $response;
			}
				return $dimensions->result_array();
		 	
		}

        		#log DB errors
		public function log_db_error($error, $service_name, $remarks = '') 
		{
			if($error AND count($error) > 0) {
				
				$db2= $this->load->database('trans', TRUE);	
				
				$data['LOG_ID'] 		= $db2->query('SELECT EVENT_APP_SEQ_ERROR_LOGID.nextVal AS LOG_ID FROM dual')->row()->LOG_ID;
				$data['APP_NAME']		= 'STO Events App';
				$data['SERVICE_NAME']	= $service_name;
				$data['ERROR_CODE']		= $error['code'];
				$data['MESSAGE']		= $error['message'];	
				$data['OFFSET']			= $error['offset'];
				$data['SQLTEXT']		= $error['sqltext'];
				$data['REMARKS']		= $remarks;
				
				@$db2->insert('EVENT_APP_ERRORS_LOG', $data);
				
				$message  = '<p> SERVICE NAME : ' . $service_name . '</p>';
				$message .= '<p> ERROR CODE : ' . $error['code'] . '</p>';
				$message .= '<p> ERROR MESSAGE : ' . $error['message'] . '<p>';
				$message .= '<p> SQL STATEMENT : <br><br> ' . $error['sqltext'] . '<p>';
				
				@$this->send_email('samee.ullah@bh.edu.pk, shahzad.rafique@bh.edu.pk, jabbar.khan@bh.edu.pk', 'SOT EVENTS APP DATABASE ERROR', $message);
			}
			
		}
}
?>