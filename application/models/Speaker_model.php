<?php

class Speaker_model extends CI_Model 
{

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
		
		public function get($participant_id = null)
		{

			$response = array();
			$photo = 'http://wstest.beaconhouse.net/sotportal/index.php/';

			$query  = 'SELECT t.event_id,
			t.schedule_id,
			t.participant_id,
			t.participant_name,
			t.gender,
			t.linkedin_url,
			t.twitter_url,
			t.photo,
			t.DESIGNATION,
			t.SUB_DESIGNATION,
			t.EXPERTIES,
			t.PROFILE_DESC,
			t.TAGS,
			t.role_id,
			t.role_desc
		 FROM
		 (
			 SELECT  event_id,
			 schedule_id,
			 participant_id,
			 participant_name,
			 gender,
			 linkedin_url,
			 twitter_url,
			 photo,
			 DESIGNATION,
			 SUB_DESIGNATION,
			 EXPERTIES,
			 PROFILE_DESC,
			 TAGS,
			 role_id,
			 role_desc,
					ROW_NUMBER() OVER (PARTITION BY participant_id ORDER BY participant_id DESC) rn
			 FROM bssdata.V_EVENT_SPEAKERS
		 ) t
		 WHERE t.rn = 1';

			if($participant_id != null){
				$query = $query.' where participant_id = '.$participant_id;
			}else{
				$query = $query.' ORDER BY participant_id';
			}

			$speakers = $this->db->query($query);

			
			if(!$speakers) 
			{
				$this->log_db_error($this->db->error(), 'speakers', '');
				
				 return $response;
			}
			if($participant_id != null){
				$response = $speakers->row_array();
			}else{
				return $speakers->result_array();
			}
			return $response;
        }


        public function speaker_videos($theme_id) 
		{

            $response = array();
            
			$photo = 'http://wstest.beaconhouse.net/sotportal/index.php/';

			$query  = 'SELECT v.*,p.* 
			FROM event_participant p , event_schedule_detail sd, event_schedule s,event_videos v,event_role r 
            WHERE v.schedule_id  = s.schedule_id 
            AND  s.schedule_id = sd.schedule_id
            AND  sd.participant_id = p.participant_id
            AND  sd.role_id = r.role_id
            AND  p.participant_id = '.$theme_id;
			
			$speakers = $this->db->query($query);

			if(!$speakers) 
			{
				$this->log_db_error($this->db->error(), 'get_speakers', '');
				return $response;
			}

            return $speakers->result_array();
		 	
		}

        		#log DB errors
		public function log_db_error($error, $service_name, $remarks = '') 
		{
			if($error AND count($error) > 0) {
				
				$db2= $this->load->database('trans', TRUE);	
				
				$data['LOG_ID'] 		= $db2->query('SELECT EVENT_APP_SEQ_ERROR_LOGID.nextVal AS LOG_ID FROM dual')->row()->LOG_ID;
				$data['APP_NAME']		= 'STO Events App';
				$data['SERVICE_NAME']	= $service_name;
				$data['ERROR_CODE']		= $error['code'];
				$data['MESSAGE']		= $error['message'];	
				$data['OFFSET']			= $error['offset'];
				$data['SQLTEXT']		= $error['sqltext'];
				$data['REMARKS']		= $remarks;
				
				@$db2->insert('EVENT_APP_ERRORS_LOG', $data);
				
				$message  = '<p> SERVICE NAME : ' . $service_name . '</p>';
				$message .= '<p> ERROR CODE : ' . $error['code'] . '</p>';
				$message .= '<p> ERROR MESSAGE : ' . $error['message'] . '<p>';
				$message .= '<p> SQL STATEMENT : <br><br> ' . $error['sqltext'] . '<p>';
				
				@$this->send_email('samee.ullah@bh.edu.pk, shahzad.rafique@bh.edu.pk, jabbar.khan@bh.edu.pk', 'SOT EVENTS APP DATABASE ERROR', $message);
			}
			
		}
}
?>