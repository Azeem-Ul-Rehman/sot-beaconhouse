<?php

class User_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function insert($data = array(), $device_identifier = '', $device_token = '')
    {

        var_dump('azeem');



        // Check if user had already been registered and signup request is from the same device
        // Generate User ID

        $db2= $this->load->database('trans', TRUE);

        $db2->trans_begin();

        $user_id = $db2->query('SELECT EVENT_APP_SEQ_USER_ID.nextVal AS USER_ID FROM dual')->row()->USER_ID;
        //$user_id = $db2->query('SELECT nvl(max(USER_ID),0)+1 USER_ID FROM EVENT_USERS')->row()->USER_ID;

        $sources 	= $data['SOURCES']; 	unset($data['SOURCES']);
        $other_desc	= $data['OTHER_DESC'];	unset($data['OTHER_DESC']);

        $data['USER_ID']	= $user_id;
        $data['QR']			= base_url().'uploads/qr_codes/'.$user_id.'.png';

        if($data['LOGIN_WITH'] == 'email') {
            if(isset($data['IMAGE']) && trim($data['IMAGE']) != '') {
                $data['IMAGE'] = base_url().'uploads/profile/'.$user_id.'.jpg?img='.time();
            }
        }

        if($data['DOB'] != '') {
            $db2->set('DATE_OF_BIRTH',"to_date('".$data['DOB']."','DD-MM-YYYY')",false);
        }

        if($data['AGE_GROUP_ID'] != '') {
            $db2->set('AGE_GROUP_ID', $data['AGE_GROUP_ID']);
        }

        if(isset($data['PASSWORD']) && $data['PASSWORD'] != '') {
            $data['PASSWORD'] = $db2->query("SELECT COMMON.enc_password('".$data['PASSWORD']."') PWD FROM dual")->row()->PWD;
        }

        $data['REGISTER_SOURCE'] = 'M';

        $status = @$db2->insert('EVENT_USERS', $data);

        if(!$status)
        {
            $this->log_db_error($db2->error(), 'log_signup_request', '');

            return false;
        }

        if($sources AND count($sources) > 0)
        {
            foreach($sources as $src)
            {
                $source = array(
                    'USER_ID'		=> $user_id,
                    'SOURCE_ID'		=> $src,
                    'OTHER_DESC'	=> ($src == 99) ? $other_desc : ''
                );

                @$db2->insert('EVENT_USER_SOURCE', $source);
            }
        }

        if($status) {

            $result = array('user_id' => $user_id, 'email' => $data['EMAIL']);
            @$this->func_log_user_event($db2, $user_id);

            if ($db2->trans_status() === FALSE)
            {
                $db2->trans_rollback();
                return false;
            }
            else
            {
                $db2->set('USER_ID', $user_id);
                $db2->set('LOGIN_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
                $db2->set('IP_ADDRESS', $this->input->ip_address());
                $db2->set('DEVICE_TOKEN', $device_token);
                $db2->set('DEVICE_IDENTIFIER', $device_identifier);
                $db2->set('AUTH_TOKEN', md5($user_id.time()));

                @$db2->insert("EVENT_APP_LOGIN_LOG");

                $db2->trans_commit();
                return  $result;
            }

        } else {
            return false;
        }
    }

    #log DB errors
    public function log_db_error($error, $service_name, $remarks = '')
    {
        if($error AND count($error) > 0) {

            $db2= $this->load->database('trans', TRUE);

            $data['LOG_ID'] 		= $db2->query('SELECT EVENT_APP_SEQ_ERROR_LOGID.nextVal AS LOG_ID FROM dual')->row()->LOG_ID;
            $data['APP_NAME']		= 'STO Events App';
            $data['SERVICE_NAME']	= $service_name;
            $data['ERROR_CODE']		= $error['code'];
            $data['MESSAGE']		= $error['message'];
            $data['OFFSET']			= $error['offset'];
            $data['SQLTEXT']		= $error['sqltext'];
            $data['REMARKS']		= $remarks;

            @$db2->insert('EVENT_APP_ERRORS_LOG', $data);

            $message  = '<p> SERVICE NAME : ' . $service_name . '</p>';
            $message .= '<p> ERROR CODE : ' . $error['code'] . '</p>';
            $message .= '<p> ERROR MESSAGE : ' . $error['message'] . '<p>';
            $message .= '<p> SQL STATEMENT : <br><br> ' . $error['sqltext'] . '<p>';

            @$this->send_email('samee.ullah@bh.edu.pk, shahzad.rafique@bh.edu.pk, jabbar.khan@bh.edu.pk', 'SOT EVENTS APP DATABASE ERROR', $message);
        }

    }


    public function func_log_user_event($db, $user_id){

        $sql = "SELECT DECODE(
					  (SELECT MAX(event_id) FROM events WHERE upper(TRIM(event_catagory) ) = 'SOT'
					  AND active                                                           = 'Y'
					  ),NULL,
					  (SELECT MAX(event_id)
					  FROM events
					  WHERE upper(TRIM(event_catagory) ) = 'SOT'
					  ),
					  (SELECT MAX(event_id)
					  FROM events
					  WHERE upper(TRIM(event_catagory) ) = 'SOT'
					  AND active                         = 'Y'
					  ) ) event_id
					FROM dual";

        $event_id = $db->query($sql)->row()->EVENT_ID;

        if($event_id)
        {
            $sql = "
				
					SELECT
						COUNT(*) CNT
					FROM
						event_user_registration
					WHERE
						event_id = '".$event_id."'
						AND user_id = '".$user_id."'
				";

            $rcount = $db->query($sql)->row()->CNT;

            if($rcount == 0)
            {
                $ip_address = $this->input->ip_address();

                $data = array(

                    'EVENT_ID' 			=> $event_id,
                    'USER_ID'			=> $user_id,
                    'REGISTER_SOURCE'	=> 'M',
                    'ENTRY_IP'			=> $this->input->ip_address()
                );

                @$db->insert('EVENT_USER_REGISTRATION', $data);

            }
        }
    }



}

?>