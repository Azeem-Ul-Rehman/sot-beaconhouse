<?php
$data_array = array(
    'email' => $_POST['email'],
    'password' => $_POST['password'],
    'first_name' => $_POST['first_name'],
    'last_name' => $_POST['last_name'],
    'date_of_birth' => $_POST['date_of_birth'],
    'age_group_id' => $_POST['age_group_id'],
    'gender' => $_POST['gender'],
    'phone' => $_POST['phone'],
    'image' => $_POST['image'],
    'login_with' => $_POST['login_with'],
    'city_id' => $_POST['city_id'],
    'location_id' => $_POST['location_id'],
    'other_location' => $_POST['other_location'],
    'is_associate' => $_POST['is_associate'],
    'association_id' => $_POST['association_id'],
    'other_association' => $_POST['other_association'],
    'sources' => $_POST['sources'],
    'other_source' => $_POST['other_source'],
    'profile_update_status' => (null != $_POST('profile_update_status')) ? $_POST('profile_update_status') : 'N',
);

if(!empty($_POST) and $_POST['ajax'] == 'ajaxLoad') {
    //echo 'ajax testing';
    $data = curlCall('user/insert_user/',json_encode($data_array), 'POST');
    $response = json_decode($data, true);
    $errors   = $response['response']['errors'];
    $data     = $response['response']['data'][0];
}?>

<script type="text/javascript">
    jQuery( window ).on( "load", function()
    {
        jQuery('#submitVideo').click(function ()
        {
            jQuery('#allvideos').fadeOut();
            jQuery('#ajaxLoader').show();

            jQuery.ajax({
		type: "POST",
		data:jQuery("#searchvideo").serialize(),
		success: function(data)
		{
            jQuery('#ajaxLoader').hide();
            jQuery('#allvideos').html(data).fadeIn();

		});
	});

    });
</script>